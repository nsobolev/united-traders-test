import React, { FC } from 'react';

type TPartner = {
  id: number;
  name: string;
  image: string;
}

export type TPartners = TPartner[];
type TPartnersProps = {
  partners: TPartners,
}

const Partners: FC<TPartnersProps> = ({ partners }) => {
  return <div className="partners">
    <ul className="partners__list">
      {
        partners.map(({ id, name, image }) => (
          <li key={id} className="partners__item">
            <img className="partners__image" src={image} alt={name} />
          </li>
        ))
      }
    </ul>
  </div>
}

export default Partners
