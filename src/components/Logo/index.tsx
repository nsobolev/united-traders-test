import React from 'react'
import LogoPNG from './assets/logo.png'

const Logo = () => {
  return <div className="logo">
    <img className="logo__image" src={LogoPNG} alt="Logo Company"/>
  </div>
}

export default Logo
