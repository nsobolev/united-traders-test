import React, { useState } from 'react'
import classNames from 'classnames'

import Logo from '../Logo'
import Menu from '../Menu'
import AuthMenu from '../AuthMenu'
import ButtonMenu from '../Common/ButtonMenu'

import { menuLinks, authMenuLinks } from './menuLinks'

const MainHeader = () => {
  const [isOpenMobileMenu, setIsOpenMobileMenu] = useState(false)

  const onToggleButtonMenu = () => {
    setIsOpenMobileMenu(prevState => !prevState)
  }

  return <header className="main-header">
    <div className="main-header__container container">
      <div className="main-header__top">
        <div className="main-header__logo">
          <Logo />
        </div>
        <div className="main-header__button-menu">
          <ButtonMenu isOpen={isOpenMobileMenu} onClick={onToggleButtonMenu}>Меню</ButtonMenu>
        </div>
        <div className={classNames('main-header__container-menu', { 'main-header__container-menu_state_open': isOpenMobileMenu })}>
          <div className="main-header__menu">
            <Menu menuLinks={ menuLinks } />
          </div>
          <div className="main-header__auth-menu">
            <AuthMenu menuLinks={ authMenuLinks } />
          </div>
        </div>
      </div>
      <div className="main-header__bottom">
        <h1 className="main-header__slogan">
          WE SOLVE <br/>
          YOUR PROBLEMS
        </h1>
        <p className="main-header__description">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc nec accumsan velit. Ut gravida
          id sem id sodales. Aenean quis enim dui. Vestibulum ullamcorper vel urna quis congue.
        </p>
      </div>
    </div>
  </header>
}

export default MainHeader
