import { TMenuLinks } from '../Menu'
import { TAuthMenuLinks } from '../AuthMenu'

export const menuLinks: TMenuLinks = [
  {
    id: 1,
    name: 'Products',
    link: '#',
    children: [],
  },
  {
    id: 2,
    name: 'News & Events',
    link: '#',
  },
  {
    id: 3,
    name: 'Support',
    link: '#',
  },
  {
    id: 4,
    name: 'Contacts',
    link: '#',
  }
]

export const authMenuLinks: TAuthMenuLinks = [
  {
    id: 1,
    name: 'Login',
    link: '#',
  },
  {
    id: 2,
    name: 'Buy Now',
    link: '#',
  }
]
