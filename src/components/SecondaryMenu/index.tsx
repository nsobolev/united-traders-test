import React, { FC } from 'react'

export type TSecondaryMenuLink = {
  id: number;
  name: string;
  link: string;
}

export type TSecondaryMenuLinks = TSecondaryMenuLink[];

type TSecondaryMenuLinksProps = {
  menuLinks: TSecondaryMenuLinks;
}

const SecondaryMenu: FC<TSecondaryMenuLinksProps> = ({ menuLinks }) => {
  return <ul className="secondary-menu menu-list">
    {
      menuLinks.map(menuLink => (
        <li key={menuLink.id} className="secondary-menu__item menu-list__item">
          <a className="secondary-menu__link menu-list__link" href={ menuLink.link }>{ menuLink.name }</a>
        </li>
      ))
    }
  </ul>
}

export default SecondaryMenu
