import React from 'react'

const Copyright = () => {
  return <div className="copyright">
    <p className="copyright__text">
      Copyright © 1999 - 2019
    </p>
  </div>
}

export default Copyright
