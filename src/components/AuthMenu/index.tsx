import React, { FC } from 'react'

export type TAuthMenuLink = {
  id: number;
  name: string;
  link: string;
}

export type TAuthMenuLinks = TAuthMenuLink[];

type TAuthMenuLinksProps = {
  menuLinks: TAuthMenuLinks;
}

const AuthMenu: FC<TAuthMenuLinksProps> = ({ menuLinks }) => {
  return <ul className="auth-menu menu-list">
    {
      menuLinks.map(menuLink => (
        <li key={menuLink.id} className="auth-menu__item menu-list__item">
          <a className="auth-menu__link menu-list__link" href={ menuLink.link }>{ menuLink.name }</a>
        </li>
      ))
    }
  </ul>

}

export default AuthMenu
