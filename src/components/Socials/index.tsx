import React, { FC } from 'react'

type TSocial = {
  id: number;
  name: string;
  link: string;
  Icon: any ;
}

export type TSocials = TSocial[];
type TSocialsProps = {
  socials: TSocials;
}

const Socials: FC<TSocialsProps> = ({ socials }) => {
  return <ul className="socials">
    {
      socials.map(({ id, name, link, Icon }) => (
        <li key={id} className="socials__item">
          <a className="socials__link" href={link}>
            <Icon className="socials__icon" />
            <span className="visually-hidden">{name}</span>
          </a>
        </li>
      ))
    }
  </ul>
}

export default Socials
