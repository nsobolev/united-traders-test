import React, { FC } from 'react';

const Button: FC = ({ children }) => {
  return <button className="button" type="button">{ children }</button>
};

export default Button;
