import React, { FC, PropsWithChildren } from 'react'
import classNames from 'classnames'

type TButtonMenuProps = {
  isOpen: boolean;
  onClick: (evt: React.MouseEvent) => void
}

const Button: FC<PropsWithChildren<TButtonMenuProps>> = ({ children, onClick, isOpen }) => {
  return <button className={classNames('button-menu', { 'button-menu_state_open': isOpen })} type="button" onClick={ onClick }>
    <span className="button-menu__line" />
    <span className="visually-hidden">{ children }</span>
  </button>
};

export default Button;
