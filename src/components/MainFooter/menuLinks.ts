import { TSecondaryMenuLinks } from '../SecondaryMenu'

export const menuLinksFooter: TSecondaryMenuLinks = [
  {
    id: 1,
    name: 'PRIVACY POLICY',
    link: '#',
  },
  {
    id: 2,
    name: 'ABOUT US',
    link: '#',
  },
  {
    id: 3,
    name: 'SUPPORT',
    link: '#',
  },
]
