import { TSocials } from '../Socials';

import { ReactComponent as FacebookSVG } from '../../svg/facebook.svg'
import { ReactComponent as TwitterSVG } from '../../svg/twitter.svg'
import { ReactComponent as InstagramSVG } from '../../svg/instagram.svg'

export const socials: TSocials = [
  {
    id: 1,
    name: 'Facebook',
    link: '#',
    Icon: FacebookSVG,
  },
  {
    id: 2,
    name: 'Twitter',
    link: '#',
    Icon: TwitterSVG,
  },
  {
    id: 3,
    name: 'Instagram',
    link: '#',
    Icon: InstagramSVG,
  }
]
