import React from 'react'
import SecondaryMenu from '../SecondaryMenu'
import Socials from '../Socials'
import Copyright from '../Copyright'

import { menuLinksFooter } from './menuLinks'
import { socials } from './socials'

const MainFooter = () => {
  return <footer className="main-footer">
    <div className="main-footer__container container">
      <div className="main-footer__menu">
        <SecondaryMenu menuLinks={ menuLinksFooter } />
      </div>
      <div className="main-footer__social">
        <Socials socials={ socials } />
      </div>
      <div className="main-footer__copyright">
        <Copyright />
      </div>
    </div>
  </footer>
}

export default MainFooter;
