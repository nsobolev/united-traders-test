import React, { FC } from 'react'
import Button from '../Common/Button'
import classNames from 'classnames'

export enum TileTheme {
  DEFAULT,
  GRAY
}

type TTile = {
  id: number;
  slogan: string;
  Icon: any;
  description: string;
  linkName?: string;
  link?: string;
}

export type TTiles = TTile[];
type TTileProps = {
  tiles: TTiles;
  buttonTitle?: string;
  theme?: TileTheme;
}

const Tile: FC<TTileProps> = ({ tiles, buttonTitle, theme }) => {
  return <div className={ classNames('tile', { 'tile_theme_gray':  theme === TileTheme.GRAY }) }>
    <ul className="tile__list">
      {
        tiles.map(({ id, slogan, Icon, description, linkName, link }) => (
          <li key={id} className="tile__item">
            <div className="tile__image-container">
              <Icon className="tile__image" />
            </div>
            <h3 className="tile__slogan">{ slogan }</h3>
            <p className="tile__description" dangerouslySetInnerHTML={{__html: description}} />
            {
              link && linkName &&
              <div className="tile__footer">
                <a className="tile__link" href={link}>{linkName}</a>
              </div>
            }
          </li>
        ))
      }
    </ul>
    {
      buttonTitle && <div className="tile__button-container">
        <Button>{ buttonTitle }</Button>
      </div>
    }
  </div>
}

Tile.defaultProps = {
  buttonTitle: '',
  theme: TileTheme.DEFAULT
}

export default Tile
