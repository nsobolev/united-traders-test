import React, { FC } from 'react'
import classNames from 'classnames'
import { ReactComponent as ArrowDownSVG } from '../../svg/arrow-down.svg'

export type TMenuLink = {
  id: number;
  name: string;
  link: string;
  children?: any[];
}

export type TMenuLinks = TMenuLink[];

type TMenuLinksProps = {
  menuLinks: TMenuLinks;
}

const Menu: FC<TMenuLinksProps> = ({ menuLinks }) => {
  return <nav className="menu">
    <ul className="menu__list menu-list">
    {
      menuLinks.map(menuLink => (
        <li key={menuLink.id} className={ classNames('menu-list__item', 'menu__item', { 'menu-list__item_view_sub-menu': Boolean(menuLink.children) }) }>
          <a className="menu__link menu-list__link" href={ menuLink.link }>
            { menuLink.name }
            { menuLink.children &&  <ArrowDownSVG className="menu__icon menu-list__icon" /> }
          </a>
        </li>
      ))
    }
    </ul>
  </nav>
}

export default Menu
