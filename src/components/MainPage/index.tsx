import React, { FC, PropsWithChildren } from 'react'

const MainPage: FC = ({ children }) => {
  return <main className="main-page">
    <div className="main-page__container container">
      {children}
    </div>
  </main>
};

type MainPageItemProps = {
  slogan: string;
  description?: string;
}

export const MainPageItem: FC<PropsWithChildren<MainPageItemProps>> = ({ slogan, children, description }) => {
  return <div className="main-page-item">
    <h2 className="main-page-item__slogan">{ slogan }</h2>
    { description && <p className="main-page-item__description">{description}</p> }
    {children}
  </div>
};

MainPageItem.defaultProps = {
  description: '',
}

export default MainPage;
