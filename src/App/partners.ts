import { TPartners } from '../components/Partners'

import Company1PNG from '../static/image/company1.png'
import Company2PNG from '../static/image/company2.png'
import Company3PNG from '../static/image/company3.png'
import Company4PNG from '../static/image/company4.png'
import Company5PNG from '../static/image/company5.png'

export const partners: TPartners = [
  {
    id: 1,
    name: 'Company 1',
    image: Company1PNG,
  },
  {
    id: 2,
    name: 'Company 2',
    image: Company2PNG,
  },
  {
    id: 3,
    name: 'Company 3',
    image: Company3PNG,
  },
  {
    id: 4,
    name: 'Company 4',
    image: Company4PNG,
  },
  {
    id: 5,
    name: 'Company 5',
    image: Company5PNG,
  }
]
