import React from 'react'
import '../styles/style.less'

import MainHeader from '../components/MainHeader'
import MainFooter from '../components/MainFooter'
import MainPage, { MainPageItem } from '../components/MainPage'
import Tile, { TileTheme } from '../components/Tile'
import Partners from '../components/Partners'

import { tiles, tilesSecondary } from './tiles'
import { partners } from './partners'

const App = () => {
  return <>
      <MainHeader />
        <MainPage>
          <MainPageItem slogan="Etiam ac nisl ac quam sodales scelerisque elementum quis purus.">
            <Tile tiles={ tiles } theme={ TileTheme.GRAY } />
          </MainPageItem>
          <MainPageItem
            slogan="Etiam ac nisl ac quam sodales scelerisque elementum quis purus."
            description="Integer bibendum turpis sit amet pretium commodo. Cras justo elit, dapibus at viverra non,
              dapibus vitae est. Morbi facilisis mi non iaculis porttitor. Phasellus a nibh a lacus
              dignissim pharetra. Vivamus ullamcorper finibus mauris, sit amet volutpat enim sollicitudin
              sed.">
            <Tile tiles={ tilesSecondary } buttonTitle="View All Our Products" />
          </MainPageItem>
          <MainPageItem slogan="Wordlwide recognition">
            <>
              <p>
                Integer bibendum turpis sit amet pretium commodo. Cras justo elit, dapibus at viverra non,
                dapibus vitae est.
              </p>
              <p>
                Fusce pretium enim sit amet elementum eleifend. Quisque at dui nunc. Curabitur sodales,
                turpis at luctus suscipit, nisl leo dignissim turpis, quis blandit justo nulla sed nisi.
                Vivamus urna eros, malesuada non massa sed, vestibulum maximus dui. Fusce facilisis nunc a
                urna lacinia, nec sagittis felis efficitur. Phasellus commodo porttitor odio, eu commodo
                nisi vehicula ut. Maecenas congue nisi lorem, et maximus ante vestibulum eu. Vivamus non
                imperdiet ex.
              </p>
              <Partners partners={partners} />
            </>
          </MainPageItem>
        </MainPage>
      <MainFooter />
    </>
}

export default App;
