import { TTiles } from '../components/Tile';

import { ReactComponent as MicroscopeSVG } from '../svg/tile/microscope.svg'
import { ReactComponent as EdificeSVG } from '../svg/tile/edifice.svg'
import { ReactComponent as HomeSVG } from '../svg/tile/home.svg'

import { ReactComponent as DocumentsSVG } from '../svg/tile/documents.svg'
import { ReactComponent as TimeSVG } from '../svg/tile/time.svg'
import { ReactComponent as FoldersSVG } from '../svg/tile/folders.svg'
import { ReactComponent as GeneratorSVG } from '../svg/tile/generator.svg'
import { ReactComponent as FileSVG } from '../svg/tile/file.svg'
import { ReactComponent as DiagramSVG } from '../svg/tile/diagram.svg'

export const tiles: TTiles = [
  {
    id: 1,
    slogan: 'Donec efficitur molestie',
    description: 'Nulla pretium nunc in nulla placerat, sit amet porttitor enim ullamcorper. Curabitur ex erat, euismod ac vestibulum vel, viverra non lorem.',
    link: '#',
    linkName: 'Learn More',
    Icon: MicroscopeSVG,
  },
  {
    id: 2,
    slogan: 'Mauris interdum venenatis',
    description: 'Vivamus cursus ante eu orci rutrum, vitae viverra ante dapibus. Vivamus porta molestie turpis, quis elementum nisi egestas eget.',
    link: '#',
    linkName: 'Learn More',
    Icon: EdificeSVG,
  },
  {
    id: 3,
    slogan: 'Cras sit amet',
    description: 'Fusce suscipit, ante at imperdiet vehicula, ex augue feugiat enim, sed faucibus sem tortor quis turpis. Mauris lacinia neque id augue efficitur, ut feugiat massa efficitur.',
    link: '#',
    linkName: 'Learn More',
    Icon: HomeSVG,
  },
]

export const tilesSecondary: TTiles = [
  {
    id: 1,
    slogan: 'Proin felis eros',
    description: 'Iverra sed eros id, eleifend maximus tortor',
    Icon: DocumentsSVG,
  },
  {
    id: 2,
    slogan: 'Suspendisse potenti',
    description: 'Vestibulum mi risus, aliquam vitae efficitur quis',
    Icon: TimeSVG,
  },
  {
    id: 3,
    slogan: 'Fusce pretium enim',
    description: 'Quisque at dui nunc. Curabitur sodales, turpis at luctus suscipit, nisl leo dignissim turpis',
    Icon: FoldersSVG,
  },
  {
    id: 4,
    slogan: 'Vivamus urna eros',
    description: 'Fusce facilisis nunc a urna lacinia, nec sagittis felis efficitur. Phasellus commodo porttitor odio',
    Icon: GeneratorSVG,
  },
  {
    id: 5,
    slogan: ' Maecenas congue nisi',
    description: 'Interdum et malesuada fames ac ante ipsum primis in faucibus. Curabitur vulputate <a href="#">arcu nisi</a>',
    Icon: FileSVG,
  },
  {
    id: 6,
    slogan: 'Nullam purus velit',
    description: 'Praesent ac turpis sit amet ex euismod congue vitae sit amet risus',
    Icon: DiagramSVG,
  }
]
